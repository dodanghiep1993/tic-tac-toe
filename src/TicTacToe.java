import javax.sound.midi.Soundbank;
import java.util.*;

public class TicTacToe {
//    ArrayList<Integer> cpuposition = new ArrayList<>();
//    ArrayList<Integer> userposition = new ArrayList<>();

    public static void main(String[] args) {
        String answer = "";
        do {
            ArrayList<Integer> cpuposition = new ArrayList<>();
            ArrayList<Integer> userposition = new ArrayList<>();
            Scanner getinput = new Scanner(System.in);
            char[][] grid = {{' ','|',' ','|', ' '},
                             {'-','-','-','-', '-'},
                             {' ','|',' ','|', ' '},
                             {'-','-','-','-', '-'},
                             {' ','|',' ','|', ' '}};
            System.out.println("\n" + "Welcome to Tic-Tac-Toe!");
            System.out.println("Do you want to be X or O?\n");

            String xory = "";
            while (true) {
                try {
                    if (getinput.hasNextInt()) {
                        throw new Exception();
                    } else {
                        xory = getinput.next();
                        break;
                    }
                } catch (Exception e) {
                    getinput.next();
                    System.out.println();
                    System.out.println();
                    System.out.println("Invalid input! input should only be X or O");
                }
            }

            System.out.println("\n" + "The computer will go first.\n");
            Random rand = new Random();

            while(true) {
                int cpu = rand.nextInt(9) + 1;
                while(userposition.contains(cpu) || cpuposition.contains(cpu)) {
                    cpu = rand.nextInt(9) + 1;
                }
                InsertXoO(grid, cpu, xory.charAt(0), "cpu", cpuposition, userposition);
                String check = checkwin(cpuposition, userposition);
                System.out.println();
                Printgrid(grid);
                if (check.length() > 0) {
                    System.out.println(check);
                    break;
                }

                System.out.println("\n" + "What is your next move? (1-9)\n");
                int user; //= getinput.nextInt();
                while (true) {
                    try {
                        user = getinput.nextInt();
                        if (user > 9) {
                            throw new Exception();
                        }
                        break;
                    } catch (Exception e) {
                        getinput.next();
                        System.out.println();
                        System.out.println();
                        System.out.println("Invalid input! input should only be from 1 to 9");
                    }
                }
                while(userposition.contains(user) || cpuposition.contains(user)) {
                    System.out.println("\n" + "Position taken! Enter a correct position\n");
                    //user = getinput.nextInt();
                    while (true) {
                        try {
                            user = getinput.nextInt();
                            if (user > 9) {
                                throw new Exception();
                            }
                            break;
                        } catch (Exception e) {
                            getinput.next();
                            System.out.println();
                            System.out.println();
                            System.out.println("Invalid input! input should only be from 1 to 9");
                        }
                    }
                }
                InsertXoO(grid, user, xory.charAt(0), "user", cpuposition, userposition);

                check = checkwin(cpuposition, userposition);
                if (check.length() > 0) {
                    System.out.println();
                    Printgrid(grid);
                    System.out.println(check);
                    break;
                }
            }
            System.out.println("\n" + "Do you want to play again? (yes or no)\n");
            //answer = getinput.next();
            while (true) {
                try {
                    if (getinput.hasNextInt()) {
                        throw new Exception();
                    } else {
                        answer = getinput.next();
                        break;
                    }
                } catch (Exception e) {
                    getinput.next();
                    System.out.println();
                    System.out.println();
                    System.out.println("Invalid input! input should only be yes or no");
                }
            }
        } while(answer.equals("yes"));

    }

    public static String checkwin(ArrayList<Integer> cpuposition, ArrayList<Integer> userposition) {
        List frow = Arrays.asList(1, 2, 3);
        List srow = Arrays.asList(4, 5, 6);
        List trow = Arrays.asList(7, 8, 9);
        List fcoloum = Arrays.asList(1, 4, 7);
        List scoloum = Arrays.asList(2, 5, 8);
        List tcoloum = Arrays.asList(3, 6, 9);
        List fcross = Arrays.asList(3, 5, 7);
        List scross = Arrays.asList(1, 5, 9);

        List<List> all = new ArrayList<>();
        all.add(frow);
        all.add(srow);
        all.add(trow);
        all.add(fcoloum);
        all.add(scoloum);
        all.add(tcoloum);
        all.add(fcross);
        all.add(scross);

        for(List l: all) {
            if(cpuposition.containsAll(l)) {
                return "\nThe computer has beaten you! You lose.";
            } else if (userposition.containsAll(l)) {
                return "\nCongratulations! You have won!";
            } else if (cpuposition.size() + userposition.size() == 9) {
                return "\nYou are as smart as the AI";
            }
        }
        return "";
    }

    public static void InsertXoO(char[][] grid, int input,char symbol, String compare, ArrayList<Integer> cpuposition, ArrayList<Integer> userposition) {
        if(symbol == 'X' && compare.equals("cpu")) {
            symbol ='O';
            cpuposition.add(input);
        } else if (symbol == 'O' && compare.equals("user")) {
            symbol ='O';
            userposition.add(input);
        } else if (symbol == 'O' && compare.equals("cpu")){
            symbol ='X';
            cpuposition.add(input);
        } else if (symbol == 'X' && compare.equals("user")) {
            symbol ='X';
            userposition.add(input);
        }
        switch (input) {
            case 1:
                grid[0][0] = symbol;
                break;
            case 2:
                grid[0][2] = symbol;
                break;
            case 3:
                grid[0][4] = symbol;
                break;
            case 4:
                grid[2][0] = symbol;
                break;
            case 5:
                grid[2][2] = symbol;
                break;
            case 6:
                grid[2][4] = symbol;
                break;
            case 7:
                grid[4][0] = symbol;
                break;
            case 8:
                grid[4][2] = symbol;
                break;
            case 9:
                grid[4][4] = symbol;
                break;
        }
    }

    public static void Printgrid(char[][] grid) {
        for(char[] charray: grid) {
            for(char charac: charray) {
                System.out.print(charac + "\t");
            }
            System.out.println();
        }
    }
}
